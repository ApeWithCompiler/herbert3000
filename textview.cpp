#include "textview.h"

#include <wx/dc.h>
#include <wx/settings.h>

void textview::set_source(line_source *new_src)
{
	if(src == new_src)
		return;
	if(src)
		src->set_observer(nullptr);
	src = new_src;
	if(src)
		src->set_observer(this);

	invalidate();
}

void textview::move_cursor(direction d)
{
	wxRect redraw;

	wxSize canvas_size = GetSize();

	switch(d)
	{
	case up:
		if(current_line == 0)
			return;
		--current_line;
		redraw = wxRect
		{
			0, wxCoord(current_line * line_height),
			canvas_size.x, wxCoord(2 * line_height)
		};
		break;
	case down:
		if(current_line == line_count - 1)
			return;
		redraw = wxRect
		{
			0, wxCoord(current_line * line_height),
			canvas_size.x, wxCoord(2 * line_height)
		};
		++current_line;
		break;
	}
	RefreshRect(redraw);
}

void textview::OnDraw(wxDC &dc)
{
	if(!src)
		return;

	wxSize canvas_size = dc.GetSize();

	auto const highlight_bg = wxSystemSettings::GetColour(
			wxSYS_COLOUR_WINDOW);

	dc.SetBrush(highlight_bg);
	dc.SetFont(*font);

	wxRect clip;

	dc.GetClippingBox(clip);

	if(line_count == 0)
		return;

	unsigned int const first_clip_line = clip.y / line_height;
	unsigned int const last_clip_line =
		(clip.y + clip.height - 1) / line_height;

	unsigned int const first_line = first_clip_line;
	unsigned int const last_line =
		(last_clip_line < line_count)
		? last_clip_line
		: line_count - 1;

	wxString text;

	for(unsigned int i = first_line; i <= last_line; ++i)
	{
		bool const is_current_line = (i == current_line);

		wxPoint const line_left_top { 0, wxCoord(i * line_height) };
		wxSize const line_size { canvas_size.x, wxCoord(line_height) };

		src->get_line(i, text);

		if(is_current_line)
			dc.DrawRectangle(
					line_left_top,
					line_size);

		dc.DrawText(text, line_left_top);
	}
}

void textview::OnSize(wxSizeEvent &)
{
	wxRect const new_size = GetClientSize();

	/* the only thing that is size dependent is the right border of the
	 * "cursor" box */

	if(size.width != new_size.width)
	{
		bool const shrinking = (new_size.width < size.width);

		wxRect const refresh =
		{
			/* x */ (shrinking ? new_size.width : size.width) - 1,
			/* y */ wxCoord(current_line * line_height),
			/* width */ 1,
			/* height */ wxCoord(line_height)
		};
		RefreshRect(refresh);
	}
	size = new_size;
}

void textview::OnFontSizeChange(unsigned int new_size)
{
	font.reset(new wxFont(new_size, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
	Refresh();
}

void textview::OnLineHeightChange(unsigned int new_height)
{
	if(!src)
	{
		SetScrollbars(0, 0, 0, 0);
		return;
	}

	wxPoint old_view_start = GetViewStart();

	unsigned int line = old_view_start.y / line_height;
	unsigned int offset = old_view_start.y % line_height;

	line_height = new_height;

	SetScrollbars(
		0, 1,
		0, line_count * line_height,
		0, line * line_height + offset);
	Refresh();
}

void textview::invalidate() noexcept
{
	line_count =
		src
		? src->get_count()
		: 0;

	SetScrollbars(
		0, 1,
		0, line_count * line_height,
		0, 0);
	Refresh();
}

BEGIN_EVENT_TABLE(textview, wxScrolledCanvas)
	EVT_SIZE(textview::OnSize)
END_EVENT_TABLE()
