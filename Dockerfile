FROM debian:bullseye
RUN [ "apt-get", "update" ]
RUN [ "apt-get", "--yes", "--no-install-recommends", "install", "g++", "meson", "libwxgtk3.0-gtk3-dev" ]

