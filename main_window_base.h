///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-0-g8feb16b)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
class textview;

#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/spinctrl.h>
#include <wx/toolbar.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class main_window_base
///////////////////////////////////////////////////////////////////////////////
class main_window_base : public wxFrame
{
	private:

	protected:
		wxMenuBar* m_menubar;
		wxMenu* m_fileMenu;
		wxToolBar* m_toolBar;
		wxSpinCtrl* m_fontSize;
		wxSpinCtrl* m_lineHeight;
		textview* m_view;

		// Virtual event handlers, override them in your derived class
		virtual void OnQuit( wxCommandEvent& event ) = 0;
		virtual void OnFontSizeChange( wxSpinEvent& event ) = 0;
		virtual void OnLineHeightChange( wxSpinEvent& event ) = 0;
		virtual void OnChar( wxKeyEvent& event ) = 0;


	public:

		main_window_base( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~main_window_base();

};

