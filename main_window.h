#pragma once

#include "main_window_base.h"

#include "source.h"
#include "line_source.h"

#include <memory>

class hexdump;

class main_window : public main_window_base
{
public:
	main_window();

	void OnQuit(wxCommandEvent &) override
	{
		Close();
	}

	void OnFontSizeChange(wxSpinEvent &) override;
	void OnLineHeightChange(wxSpinEvent &) override;

	void OnChar(wxKeyEvent &) override;

	void set_file(wxString const &file);

private:
	wxString file;

	std::unique_ptr<source> src;
	std::unique_ptr<hexdump> hex;
};
