///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-0-g8feb16b)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "textview.h"

#include "main_window_base.h"

///////////////////////////////////////////////////////////////////////////

main_window_base::main_window_base( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	m_menubar = new wxMenuBar( 0 );
	m_fileMenu = new wxMenu();
	wxMenuItem* m_quitItem;
	m_quitItem = new wxMenuItem( m_fileMenu, wxID_EXIT, wxString( wxT("Quit") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_quitItem->SetBitmaps( wxArtProvider::GetBitmap( wxART_QUIT, wxART_MENU ), wxNullBitmap );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_quitItem->SetBitmap( wxArtProvider::GetBitmap( wxART_QUIT, wxART_MENU ) );
	#endif
	m_fileMenu->Append( m_quitItem );

	m_menubar->Append( m_fileMenu, wxT("File") );

	this->SetMenuBar( m_menubar );

	m_toolBar = this->CreateToolBar( wxTB_HORIZONTAL, wxID_ANY );
	m_fontSize = new wxSpinCtrl( m_toolBar, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 10, 48, 14 );
	m_toolBar->AddControl( m_fontSize );
	m_lineHeight = new wxSpinCtrl( m_toolBar, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 10, 48, 16 );
	m_toolBar->AddControl( m_lineHeight );
	m_toolBar->Realize();

	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxVERTICAL );

	m_view = new textview( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxALWAYS_SHOW_SB|wxTAB_TRAVERSAL|wxVSCROLL|wxWANTS_CHARS );
	bSizer1->Add( m_view, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( bSizer1 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_fileMenu->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( main_window_base::OnQuit ), this, m_quitItem->GetId());
	m_fontSize->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( main_window_base::OnFontSizeChange ), NULL, this );
	m_lineHeight->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( main_window_base::OnLineHeightChange ), NULL, this );
	m_view->Connect( wxEVT_CHAR, wxKeyEventHandler( main_window_base::OnChar ), NULL, this );
}

main_window_base::~main_window_base()
{
	// Disconnect Events
	m_fontSize->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( main_window_base::OnFontSizeChange ), NULL, this );
	m_lineHeight->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( main_window_base::OnLineHeightChange ), NULL, this );
	m_view->Disconnect( wxEVT_CHAR, wxKeyEventHandler( main_window_base::OnChar ), NULL, this );

}
