#pragma once

#include "source.h"

#include <Windows.h>

class windows_mmap_source :
	public source
{
public:
	windows_mmap_source(char const *file);
	~windows_mmap_source() noexcept;

	size_t get_size() const noexcept;

	void *get_mapping() const noexcept { return mapping; }

private:
	HANDLE file_handle;
	HANDLE mapping_handle;
	void *mapping;
	SIZE_T mapping_size;
};
