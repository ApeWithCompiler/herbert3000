#pragma once

#include <cstdint>

// 4294967296 lines should be enough for everyone.
typedef std::uint32_t line_number;
