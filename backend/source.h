#pragma once

#include <cstddef>

class source
{
public:
	virtual ~source() noexcept { }

	virtual size_t get_size() const = 0;

	virtual void *get_mapping() const = 0;
};
