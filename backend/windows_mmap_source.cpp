#include "windows_mmap_source.h"

windows_mmap_source::windows_mmap_source(char const *file)
{
	file_handle = ::CreateFileA(
		file,
		GENERIC_READ,
		FILE_SHARE_READ|FILE_SHARE_DELETE,
		nullptr,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		nullptr);

	if (file_handle == INVALID_HANDLE_VALUE)
		throw;

	DWORD file_size_high, file_size_low = ::GetFileSize(file_handle, &file_size_high);

	mapping_handle = ::CreateFileMapping(
		file_handle,
		nullptr,
		PAGE_READONLY,
		file_size_high,
		file_size_low,
		nullptr);

	if (mapping_handle == INVALID_HANDLE_VALUE)
		throw;

	mapping_size = SIZE_T(file_size_high) << 32 | file_size_low;

	mapping = ::MapViewOfFile(
		mapping_handle,
		FILE_MAP_READ,
		0,
		0,
		mapping_size);

	if (mapping == nullptr)
		throw;
}

windows_mmap_source::~windows_mmap_source() noexcept
{
	::UnmapViewOfFile(mapping);
	::CloseHandle(mapping_handle);
	::CloseHandle(file_handle);
}

size_t windows_mmap_source::get_size() const noexcept
{
	return mapping_size;
}
